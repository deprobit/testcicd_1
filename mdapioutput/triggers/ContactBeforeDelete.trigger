trigger ContactBeforeDelete on Contact (before Delete) {
    for(Contact c:trigger.old)
    {
        if(c.accountId==null)
        {
            c.addError('You are not authorized to delete this Contact');
        }
    }

}