/**
* @author       
* @description  This interface is used to process the apex class, Method and payload.
*
* Modification Log
* ------------------------------------------------------------------------------------
*
**/
public interface IService {
    ServiceResponse process(ServiceRequest request);
}